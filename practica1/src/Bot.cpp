#include "DatosMemCompartida.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
int main()
{
	DatosMemCompartida* p_inicio;
	int descriptor_inicio;
	void *ini;
	struct stat bstat;

	//creación y apertura del fichero compartido
	descriptor_inicio=open("/tmp/ficherocompartido", O_RDWR,0777);
	if(descriptor_inicio==-1)
	{
		perror("ERROR APERTURA FICHERO COMPARTIDO");
		exit(1);
	}

	//creación de la estructura struct stat (estructura con info del fichero)
	fstat(descriptor_inicio,&bstat);

	//proyección del fichero en memoria
	ini=mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE | PROT_READ,MAP_SHARED,descriptor_inicio,0);
	//asignación del puntero a la posición iniciales
	p_inicio=(DatosMemCompartida*)ini;
	//cierre del fichero
	close(descriptor_inicio);
	
	while(1)
	{
		if(p_inicio->esfera.centro.y == (p_inicio->raqueta1.y2 + p_inicio->raqueta1.y1)/2)
			p_inicio->accion=0;
	
		if(p_inicio->esfera.centro.y < (p_inicio->raqueta1.y2 + p_inicio->raqueta1.y1)/2)
			p_inicio->accion=-1;
	
		if(p_inicio->esfera.centro.y > (p_inicio->raqueta1.y2 + p_inicio->raqueta1.y1)/2)
			p_inicio->accion=1;
	}
	usleep(25000);
}	
