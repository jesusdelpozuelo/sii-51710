// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include "Mundo.h"
#include "glut.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>
#include <unistd.h>
#include <sys/fcntl.h>
#define LONG 100
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#define MAXESFERAS 1
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

CMundo::CMundo()
{
	Init();
}

CMundo::~CMundo()
{
	
	char cadenaFinJuego[16];
	sprintf(cadenaFinJuego,"Fin del juego\n"); /* retorna el numero de caracteres escritos en el array sin contar el caracter nulo final*/
	write(fd,cadenaFinJuego,strlen(cadenaFinJuego)+1);
 	close(fd);    /*Cerramos la tuberia*/


}

void CMundo::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//		AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for (int i=0;i<ListaEsferas.size();i++)
	ListaEsferas[i].Dibuja();
	
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{	
	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	for(int i=0;i<ListaEsferas.size();i++)
		ListaEsferas[i].Mueve(0.025);
	

	
	for(int j=0;j<paredes.size();j++){
		for(int i=0;i<ListaEsferas.size();i++){
			paredes[j].Rebota(ListaEsferas[i]);
			paredes[j].Rebota(jugador1);
			paredes[j].Rebota(jugador2);
		}
	}
	
		
	

		for(int i=0;i<ListaEsferas.size();i++){
		jugador1.Rebota(ListaEsferas[i]);
		jugador2.Rebota(ListaEsferas[i]);
		}
		
			for(int i=0;i<ListaEsferas.size();i++){
				if(fondo_izq.Rebota(ListaEsferas[i]))
				{
				
				ListaEsferas[i].centro.x=0;
				ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
				puntos2++;
				
				char cadena2[LONG];
				int m;
				m=sprintf(cadena2,"Jugador 2 marca 1 punto, lleva un total de %d puntos\n",puntos2);	 /* retorna el numero de caracteres escritos en el array sin contar el caracter nulo final*/
				
				write(fd,cadena2,m+1);
				
				

					if(ListaEsferas.size()<MAXESFERAS){
					Esfera aux;
					ListaEsferas.push_back(aux);
					}
				}
				
				if(fondo_dcho.Rebota(ListaEsferas[i]))
				{
				ListaEsferas[i].centro.x=0;
				ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
				puntos1++;
				
				
      				char cadena1[LONG];
				int n;
				n=sprintf(cadena1,"Jugador 1 marca 1 punto, lleva un total de %d puntos\n",puntos1);	 /* retorna el numero de caracteres escritos en el array sin contar el caracter nulo final*/
				write(fd,cadena1,n+1);
  				
					if(ListaEsferas.size()<MAXESFERAS){
					Esfera aux;
					ListaEsferas.push_back(aux);
					}
				}
				
			}
for(int i=0;i<ListaEsferas.size();i++){
//Toma de los datos en cada instante
	dato_p->esfera.centro.y=ListaEsferas[i].centro.y;
}
	dato_p->raqueta1.y2=jugador1.y2;
	dato_p->raqueta1.y1=jugador1.y1;
	
	
	//llamada al OnKeyboardDown 
	if(dato_p->accion==1)
		OnKeyboardDown('w',0,0);
	if(dato_p->accion==0)
		OnKeyboardDown('0',0,0);
	if(dato_p->accion==-1)
		OnKeyboardDown('s',0,0);
	
	if(puntos1==2 || puntos2==2)exit(1); /* el juego finaliza cuando alguno de los jugadores llega a dos puntos*/
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
	switch(key)
	{
//	case 'a':jugador1.velocidad.x=-1;break;
//	case 'd':jugador1.velocidad.x=1;break;
	case 's':jugador1.velocidad.y=-4;break;
	case 'w':jugador1.velocidad.y=4;break;
	case 'l':jugador2.velocidad.y=-4;break;
	case 'o':jugador2.velocidad.y=4;break;

	}
}

void CMundo::Init()
{
//Creacion esfera inicial
Esfera aux;
ListaEsferas.push_back(aux);
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;

	fd=open("/home/jesus/tuberia",O_WRONLY);
	if(fd==-1){
		perror("Error al abrir la tubería\n");
		exit(1);
	}
	//creación y apertura del fichero compartido
	d_fichcomp=open("/tmp/ficherocompartido", O_CREAT|O_TRUNC|O_RDWR,0777);
	if(descriptor==-1)
	{
		perror("ERROR APERTURA FICHERO COMPARTIDO");
		exit(1);
	}

	//establecimiento del tamaño del fichero
	if (ftruncate(d_fichcomp,sizeof(dato))<0)
	{
		perror("ERROR AL ESTABLECER EL TAMAÑO DEL FICHERO");
		exit(1);
	}
	//proyección del fichero en memoria

	inicio=mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE | PROT_READ,MAP_SHARED,d_fichcomp,0);

	//cierre del descriptor de fichero compartido
	close(d_fichcomp);

	//asignación de la dirección de inicio del fichero compartido a dato_p

	dato_p=(DatosMemCompartida*)inicio;
}
