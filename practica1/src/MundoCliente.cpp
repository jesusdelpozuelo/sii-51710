// Mundo.cpp: implementation of the CMundo class.
//
//////////////////////////////////////////////////////////////////////
#include "MundoCliente.h"
#include "glut.h"
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/file.h>
#include <sys/mman.h>
#include <fcntl.h>
#include <fstream>
#include <unistd.h>
#include <sys/fcntl.h>
#define LONG 100
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#define MAXESFERAS 1

#ifdef __APPLE__
#include <OpenGL/gl.h>
#include <Glut/glut.h>
#elif defined(_WIN32) || defined(_WIN64)
#include <GLUT/glcmolorut.h>
#else
#include <GL/glut.h>
#endif
//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MundoCliente::MundoCliente()
{
 printf("He entrado al constructor del MundoCliente\n");
	Init();
}

MundoCliente::~MundoCliente()
{
printf("Destructor de cliente\n");
	char cadenaFin[16];
sprintf(cadenaFin,"Fin del juego\n"); //Para asegurar que lo que haya en cadenaFin sea un string	
	char cadenaFinJuego[16];
	sprintf(cadenaFinJuego,"Fin del juego\n"); /* retorna el numero de caracteres escritos en el array sin contar el caracter nulo final*/
	write(fd,cadenaFinJuego,strlen(cadenaFinJuego)+1);
 	close(fd);    /*Cerramos la tuberia*/

unlink("/tmp/DatosMemComp");


	//Cerrar la tubería de coordenadas
	close(fd_coordenadas);
	unlink("/home/jesus/tuberiamundoclientecoordenadas");

	//Cerrar la tubería de teclas
	write(fd_teclas,cadenaFin,strlen(cadenaFin)+1);
	close(fd_teclas);
	unlink("/home/jesus/tuberiamundoclienteteclas");

}



void MundoCliente::InitGL()
{
	//Habilitamos las luces, la renderizacion y el color de los materiales
	glEnable(GL_LIGHT0);
	glEnable(GL_LIGHTING);
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_COLOR_MATERIAL);
	
	glMatrixMode(GL_PROJECTION);
	gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
	glDisable (GL_LIGHTING);

	glMatrixMode(GL_TEXTURE);
	glPushMatrix();
	glLoadIdentity();

	glMatrixMode(GL_PROJECTION);
	glPushMatrix();
	glLoadIdentity();
	gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

	glMatrixMode(GL_MODELVIEW);
	glPushMatrix();
	glLoadIdentity();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);
	glColor3f(r,g,b);
	glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
	int len = strlen (mensaje );
	for (int i = 0; i < len; i++) 
		glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
		
	glMatrixMode(GL_TEXTURE);
	glPopMatrix();

	glMatrixMode(GL_PROJECTION);
	glPopMatrix();

	glMatrixMode(GL_MODELVIEW);
	glPopMatrix();

	glEnable( GL_DEPTH_TEST );
}
void MundoCliente::OnDraw()
{
	//Borrado de la pantalla	
   	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	//Para definir el punto de vista
	glMatrixMode(GL_MODELVIEW);	
	glLoadIdentity();
	
	gluLookAt(0.0, 0, 17,  // posicion del ojo
		0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
		0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

	/////////////////
	///////////
	//	
	
//AQUI EMPIEZA MI DIBUJO
	char cad[100];
	sprintf(cad,"Jugador1: %d",puntos1);
	print(cad,10,0,1,1,1);
	sprintf(cad,"Jugador2: %d",puntos2);
	print(cad,650,0,1,1,1);
	int i;
	for(i=0;i<paredes.size();i++)
		paredes[i].Dibuja();

	fondo_izq.Dibuja();
	fondo_dcho.Dibuja();
	jugador1.Dibuja();
	jugador2.Dibuja();
	for (int i=0;i<ListaEsferas.size();i++)
	ListaEsferas[i].Dibuja();
	
	/////////////////
	///////////
	//		AQUI TERMINA MI DIBUJO
	////////////////////////////

	//Al final, cambiar el buffer
	glutSwapBuffers();
}

void MundoCliente::OnTimer(int value)
{	

    

	jugador1.Mueve(0.025f);
	jugador2.Mueve(0.025f);
	
	for(int i=0;i<ListaEsferas.size();i++)
		ListaEsferas[i].Mueve(0.025);
	

	
	for(int j=0;j<paredes.size();j++){
		for(int i=0;i<ListaEsferas.size();i++){
			paredes[j].Rebota(ListaEsferas[i]);
			paredes[j].Rebota(jugador1);
			paredes[j].Rebota(jugador2);
		}
	}
	
		
	

		for(int i=0;i<ListaEsferas.size();i++){
		jugador1.Rebota(ListaEsferas[i]);
		jugador2.Rebota(ListaEsferas[i]);
		}
		
			for(int i=0;i<ListaEsferas.size();i++){
				if(fondo_izq.Rebota(ListaEsferas[i]))
				{
				
				ListaEsferas[i].centro.x=0;
				ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.x=2+2*rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.y=2+2*rand()/(float)RAND_MAX;
				puntos2++;
				
				

					if(ListaEsferas.size()<MAXESFERAS){
					Esfera aux;
					ListaEsferas.push_back(aux);
					}
				}
				
				if(fondo_dcho.Rebota(ListaEsferas[i]))
				{
				ListaEsferas[i].centro.x=0;
				ListaEsferas[i].centro.y=rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.x=-2-2*rand()/(float)RAND_MAX;
				ListaEsferas[i].velocidad.y=-2-2*rand()/(float)RAND_MAX;
				puntos1++;
				
				
      				
  				
					if(ListaEsferas.size()<MAXESFERAS){
					Esfera aux;
					ListaEsferas.push_back(aux);
					}
				}
				
			}
	
for(int i=0;i<ListaEsferas.size();i++){
//Toma de los datos en cada instante
	dato_p->esfera.centro.y=ListaEsferas[i].centro.y;
}
	dato_p->raqueta1.y2=jugador1.y2;
	dato_p->raqueta1.y1=jugador1.y1;
	
	
	//llamada al OnKeyboardDown 
	if(dato_p->accion==1)
		OnKeyboardDown('w',0,0);
	if(dato_p->accion==0)
		OnKeyboardDown('0',0,0);
	if(dato_p->accion==-1)
		OnKeyboardDown('s',0,0);
	


	if(puntos1==2 || puntos2==2)
	exit(1); /* el juego finaliza cuando alguno de los jugadores llega a dos puntos*/
//Leer datos de la tubería




	int read_error=read(fd_coordenadas,cad,sizeof(cad));
	if(read_error==-1){
		perror("Error al leer la tubería(MundoCliente.cpp)\n");
		exit(1);
	}
	else if(read_error==0){
	 perror("He llegado al final de la cadena de coordenadas(Estoy en MundoCliente)\n");
      exit(1);
	}


	//Actualizar los atributos de MundoCliente con los datos recibidos (sscanf()).
	sscanf(cad,"%f %f %f %f %f %f %f %f %f %f %d %d", &jugador1.x1,&jugador1.x1, &jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2, &jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1, &puntos2); 

}

void MundoCliente::OnKeyboardDown(unsigned char key, int x, int y)
{
//Escribir la tecla en la tubería 
int h2;
	h2=sprintf(cad_teclas,"%c",key);

	write(fd_teclas,cad_teclas,h2+1);
}

void MundoCliente::Init()
{

printf("He entrado al init del MundoCliente\n");
//Creacion esfera inicial
Esfera aux;
Esfera esfera;
ListaEsferas.push_back(aux);
ListaEsferas[0].centro.x=0;
ListaEsferas[0].centro.y=0;
	Plano p;
//pared inferior
	p.x1=-7;p.y1=-5;
	p.x2=7;p.y2=-5;
	paredes.push_back(p);

//superior
	p.x1=-7;p.y1=5;
	p.x2=7;p.y2=5;
	paredes.push_back(p);

	fondo_izq.r=0;
	fondo_izq.x1=-7;fondo_izq.y1=-5;
	fondo_izq.x2=-7;fondo_izq.y2=5;

	fondo_dcho.r=0;
	fondo_dcho.x1=7;fondo_dcho.y1=-5;
	fondo_dcho.x2=7;fondo_dcho.y2=5;

	//a la izq
	jugador1.g=0;
	jugador1.x1=-6;jugador1.y1=-1;
	jugador1.x2=-6;jugador1.y2=1;

	//a la dcha
	jugador2.g=0;
	jugador2.x1=6;jugador2.y1=-1;
	jugador2.x2=6;jugador2.y2=1;
	


	//creación y apertura del fichero compartido
	unlink("tmp/ficherocompartido");
	int d_fichcomp= mkfifo("tmp/ficherocompartido",0777);
	d_fichcomp=open("/tmp/ficherocompartido", O_CREAT|O_TRUNC|O_RDWR,0777);
	if(descriptor==-1)
	{
		perror("ERROR APERTURA FICHERO COMPARTIDO(MundoCliente.cpp");
		exit(1);
	}
	else{
		printf("Correcta apertura de fichero compartido(MundoCliente.cpp\n");
	}


	//establecimiento del tamaño del fichero
	if (ftruncate(d_fichcomp,sizeof(dato))<0)
	{
		perror("ERROR AL ESTABLECER EL TAMAÑO DEL FICHERO(MundoCliente.cpp");
		exit(1);
	}
	

	//proyección del fichero en memoria

	inicio=(char*)mmap(NULL,sizeof(DatosMemCompartida),PROT_WRITE | PROT_READ,MAP_SHARED,d_fichcomp,0);

	//cierre del descriptor de fichero compartido
	close(d_fichcomp);

	//asignación de la dirección de inicio del fichero compartido a dato_p

	dato_p=(DatosMemCompartida*)inicio;


	
	// CREACIÓN DE FIFO PARA COORDENADAS 

	unlink("/tmp/tuberiacoor"); //Para eliminarlo si ya existe
	int create_coordenadas=mkfifo("/tmp/tuberiacoor",0777);
	
	if(create_coordenadas==-1){ 
		perror("Error al crear la tubería de las coordenadas(MundoCliente.cpp)\n");
		exit(1);
	}
	else{
	printf("Correcta creacion de tuberia coordenadas \n");
	}




// CREACIÓN DE FIFO PARA TECLAS

	unlink("/home/jesus/tuberiamundoclienteteclas"); //Para eliminarlo si ya existe

	int error_teclas= mkfifo("/home/jesus/tuberiamundoclienteteclas",0777);
	if(error_teclas==-1){ 
		perror("Error al crear la tubería de las teclas(MundoCliente.cpp\n");		
		exit(1);
	}

	else{
	printf("Correcta creacion de tuberia teclas \n");
	}



	
	//ABRIR TUBERIA PARA COORDENADAS                 
	int fd_coordenadas=open("/tmp/tuberiacoor", O_RDONLY);
	    if(fd_coordenadas==-1){
        perror("error al abrir fifo que recibe las coordenadas(Estoy en MundoCliente)\n");
        exit(1);
    }
    else
        printf("he abierto correctament el fifo de coordenadas(Estoy en MundoCliente)\n");
    


	



	//ABRIR TUBERIA PARA TECLAS                 
   int fd_teclas=open("/home/jesus/tuberiamundoclienteteclas",O_WRONLY);
    printf("El valor de fd_fifoTeclas es %d\n",fd_teclas);
    
    if(fd_teclas==-1){
        perror("error al abrir fifo que envia las teclas(Estoy en MundoCliente)\n");
        exit(1);
    }
    else
        printf("he abierto correctament el fifo de teclas(Estoy en MundoCliente)\n");

}

